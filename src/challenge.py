import json
from uuid import UUID
import datetime

csvFilePath = 'main/resources/data.csv'
jsonFilePath = 'main/resources/data.json'
resultsFile = 'results.json'

class Device(object):
    """Represents a device with a UUID identifier,
    a location (latitude / longitude) and a timestamp. Class static methods handle
    parsing from a dictionary or csv input to create and initialize anew instance
    while verifying that the values are in the correct formats."""

    @staticmethod
    def makeFromDict(d):
        """Create a new Device instance using dictionary data"""
        dev = Device()
        success = dev._fillFromDict(d)
        if success:
            return dev
        else:
            return None

    @staticmethod
    def makeFromCSVLine(line):
        """Create a new Device instance using CSV data"""
        dev = Device()
        success = dev._fillFromCSVLine(line)
        if success:
            return dev
        else:
            return None

    def __init__(self):
        self._latitude = None
        self._longitude = None
        self._timestamp = None
        self._id = None

    def _fillFromDict(self, d):
        """Parses from a dictionary to fill the Device instance"""
        try:
            UUID(d['uuid']) #Validate that the UUID is OK
            self._id = d['uuid']
            self._latitude = float(d['lat'])
            self._longitude = float(d['long'])
            self._timestamp = int(d['timestamp'])
            return True
        except ValueError:
            print("Badly formed line")
            return False

    def _fillFromCSVLine(self, line):
        """Parses from a CSV to fill the Device instance"""
        pieces = line.strip().split(",")
        try:
            UUID(pieces[0]) #Validate that the UUID is OK
            self._id = pieces[0]
            self._latitude = float(pieces[1])
            self._longitude = float(pieces[2])
            self._timestamp = int(pieces[3])
            return True
        except ValueError:
            print("Badly formed line")
            return False

    def isInArea(self, lat1 = 89.99, long1 = -179.99, lat2 = 0.0, long2 = 0.0):
        """Detects whether a device is in given latitude / longitude bounds"""
        if ((self._latitude < lat1 and self._latitude > lat2) \
            or (self._latitude > lat1 and self._latitude < lat2)) \
            and ((self._longitude < long1 and self._longitude > long2) \
            or (self._longitude > long1 and self._longitude < long2)):
            return True
        return False

    @property
    def id(self):
        return self._id

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def latitude(self):
        return self._latitude

    @property
    def longitude(self):
        return self._longitude


class DeviceMetrics(object):
    """Runs metrics on input devices and can write those metrics to a json file."""
    def __init__(self):
        self.data = {"oldestDeviceTime":None,
                     "newestDeviceTime": None,
                     "oldestDeviceID": None,
                     "newestDeviceID" : None,
                     "totalDevices": 0,
                     "numDevicesInArea": 0}
        # Hold the device IDs we have seen in a hash table for quick checks
        self.deviceIDs = {} # id : [timestamps]

    @property
    def oldestDeviceTime(self):
        return self.data["oldestDeviceTime"]

    @property
    def newestDeviceTime(self):
        return self.data["newestDeviceTime"]

    @property
    def oldestDeviceID(self):
        return self.data["oldestDeviceID"]

    @property
    def newestDeviceId(self):
        return self.data["newestDeviceID"]

    @property
    def totalDevices(self):
        return self.data["totalDevices"]

    @property
    def uniqueDevices(self):
        return len(self.data)

    @property
    def numDevicesInArea(self):
        return self.data["numDevicesInArea"]

    def processDevice(self, device):
        """Processes a given device object and updates internal metrics"""
        if device.id in self.deviceIDs and device.timestamp in self.deviceIDs[device.id]:
            print("Dropping duplicate device / time entry.")
            return

        #Update device timestamp metrics
        self._updateDeviceTimes(device)

        #Update device counts
        self.data["totalDevices"] += 1
        if device.id not in self.deviceIDs:
            self.deviceIDs[device.id] = []
        self.deviceIDs[device.id].append(device.timestamp)
        if device.isInArea():
            self.data["numDevicesInArea"] += 1

    def _updateDeviceTimes(self, device):
        """Updates the internal oldest and newest times, IDs.
        Not sure why I pulled this out into a separate function now that I
        think about it... but why not?"""
        if self.data["oldestDeviceTime"] is None \
            or device.timestamp < self.data["oldestDeviceTime"]:
            # print("Old oldDeviceTime: %s newTime: %s" %
            #       (self.data["oldestDeviceTime"], device.timestamp) )
            self.data["oldestDeviceTime"] = device.timestamp
            self.data["oldestDeviceID"] = device.id

        if self.data["newestDeviceTime"] is None \
            or device.timestamp > self.data["newestDeviceTime"]:
            # print("Old newDeviceTime: %s newTime: %s" %
            #       (self.data["newestDeviceTime"], device.timestamp) )
            self.data["newestDeviceTime"] = device.timestamp
            self.data["newestDeviceID"] = device.id

    def writeReport(self, filePath = resultsFile):
        """Write out the current metrics to a given file path as a JSON blob"""
        outData = dict(self.data)
        baseTime = datetime.datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0)
        outData["uniqueDevices"] = len(self.deviceIDs)
        outData["newestDeviceTime"] = str(baseTime + datetime.timedelta(seconds=outData["newestDeviceTime"]))
        outData["oldestDeviceTime"] = str(baseTime + datetime.timedelta(seconds=outData["oldestDeviceTime"]))
        with open(filePath, 'w') as outFile:
            outFile.write(json.dumps(outData,
                                     sort_keys=True,
                                     indent=4,
                                     separators=(',', ': ')) + "\n")

if __name__ == "__main__":
    #Parse CSV and JSON inputs to make Device object instances
    #And do metrics on them.

    metrics = DeviceMetrics()
    print("Parsing JSON")

    with open(jsonFilePath, 'r') as jsonFile:
        #Iterate through the sub-dictionaries in the parsed json for each device
        val = json.loads(jsonFile.read())
        for jso in val['devices']:
            dev = Device.makeFromDict(jso)
            if dev:
                metrics.processDevice(dev)

    print("JSON done. Parsing CSV")

    with open(csvFilePath, 'r') as csvFile:
        #Iterate through the lines; drop the first line
        # because it's the header
        lines = csvFile.readlines()
        for line in lines[1:]:
            dev = Device.makeFromCSVLine(line)
            if dev:
                metrics.processDevice(dev)

    print("Parsing CSV done. Writing report...")
    #Dataset metrics complete. Let's write out what we need.
    metrics.writeReport()
    print("Pretty printing report complete. Thanks for using the challenge parser!")